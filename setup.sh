#!/usr/bin/env bash

# install Vundle package manager
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# Setup undo history
mkdir -p ~/.vim/undo/
